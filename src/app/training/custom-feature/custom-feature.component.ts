import { Component, OnInit, ViewChild } from '@angular/core';
import { CountUpDirective } from './../../shared/count-up/count-up.directive';
import { CountUpOptions } from 'countup.js';
@Component({
  selector: 'app-custom-feature',
  templateUrl: './custom-feature.component.html',
  styleUrls: ['./custom-feature.component.scss']
})


export class CustomFeatureComponent implements OnInit {
  @ViewChild('connectionsCounter', { read: CountUpDirective, static: false })
  connectionsCounterRef: CountUpDirective;

  avatar: string;
  avatarList: Array<string>;
  isLoading: boolean;
  dragAndDropItems: any;
  stats = {
    connections: 30000,
    destinations: 5000
  };

  constructor() {
    this.avatar = 'assets/image/avatar.jpeg';
    this.avatarList = [
      'assets/image/avatar.jpeg',
      'assets/image/avatar1.png',
      'assets/image/avatar2.png',
      null,
      'assets/image/avatar.jpeg',
      null,
      'assets/image/avatar.jpeg',
      'assets/image/avatar1.png',
      'assets/image/avatar2.png',
      null,
      'assets/image/avatar.jpeg',
      null,
      'assets/image/avatar.jpeg',
      'assets/image/avatar1.png',
      'assets/image/avatar2.png',
      null,
      'assets/image/avatar.jpeg',
      null
    ];
    this.dragAndDropItems = [
      {
        url: 'assets/image/avatar.jpeg',
        name: 'Ivan'
      },
      {
        url: 'assets/image/avatar1.png',
        name: 'Jack'
      },
      {
        url: 'assets/image/avatar2.png',
        name: 'Bart'
      }
    ];
  }
  readonly statsCounterOptions: CountUpOptions = {
    duration: 5 // measures in seconds
  };

  ngOnInit() {
  }

  onClickLoadingButton() {
    this.isLoading = true;
    setTimeout(() => {
      this.isLoading = false;
    }, 3000);
  }

  incrementConnectionsStats() {
    this.connectionsCounterRef.updateValue(1);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomFeatureComponent } from './custom-feature.component';

describe('CustomFeatureComponent', () => {
  let component: CustomFeatureComponent;
  let fixture: ComponentFixture<CustomFeatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomFeatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

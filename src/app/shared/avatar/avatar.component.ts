import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


export enum AvatarSize {
  ExtraSmall = 'xs',
  Small = 'sm',
  Medium = 'md',
  MediumUp = 'md-up',
  Large = 'lg'
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent implements OnInit {
  @Input()
  public borderWhite: boolean;

  @Input()
  public cursor: boolean;

  @Input()
  public size: AvatarSize;

  @Input()
  public outline: boolean;

  @Input()
  private source: string;

  private readonly defaultSource: string;

  constructor(private sanitizer: DomSanitizer) {
    this.defaultSource = '/assets/image/profile.svg';
  }

  public ngOnInit() {
    if (this.size == null) {
      this.size = AvatarSize.Medium;
    }
  }

  public isSourceMissing() {
    return (
      this.source == null ||
      (typeof this.source === 'string' && this.source.length < 1)
    );
  }

  public getAvatarStyles() {
    const styles = `url("${
      this.isSourceMissing() ? this.defaultSource : this.source
    }")`;
    return this.sanitizer.bypassSecurityTrustStyle(styles);
  }

  public getAvatarClass(mainClass: string) {
    return [
      mainClass,
      `avatar-${this.size}`,
      this.outline ? `avatar-outline` : '',
      this.borderWhite ? `border-white` : '',
      this.cursor ? `avatar-cursor-pointer` : '',
      this.isSourceMissing() ? 'avatar-default' : ''
    ];
  }
}

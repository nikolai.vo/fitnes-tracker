import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone,
  OnDestroy,
  ChangeDetectorRef,
  Output
} from '@angular/core';
import { Observable, interval, Subject, of, from } from 'rxjs';
import { switchMap, map, takeUntil } from 'rxjs/operators';

export interface User {
  urlUserAvatar: string;
  userName: string;
}

@Component({
  selector: 'latest-activity',
  templateUrl: './latest-activity.component.html',
  styleUrls: ['./latest-activity.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LatestActivityComponent implements OnInit, OnDestroy {
  @Output()
  public readonly onActiveActivityChange: Subject<number>;

  public activities$: Observable<Array<any>>;
  public activeActivityIndex: number;
  public userList: Array<User>;
  /**
   * @optional
   * Updates frequency in miliseconds
   */
  private readonly interval: number;
  private readonly onDestroy$ = new Subject<void>();

  constructor(
    private zone: NgZone,
    private cdRef: ChangeDetectorRef
  ) {
    this.interval = 3000;
    this.onActiveActivityChange = new Subject<number>();
    this.userList = [
      {
        urlUserAvatar: '/assets/image/avatar.jpeg',
        userName: 'Nik'
      },
      {
        urlUserAvatar: '/assets/image/avatar1.png',
        userName: 'Tom'
      },
      {
        urlUserAvatar: '/assets/image/avatar2.png',
        userName: 'Adam'
      },
      {
        urlUserAvatar: '/assets/image/profile.svg',
        userName: 'Hanna'
      },
      {
        urlUserAvatar: '/assets/image/avatar.jpeg',
        userName: 'Pitbool'
      }
    ];
  }

  ngOnInit() {
    this.activities$ = of(this.userList);
    // const outsideAngularScheduler = this.zone.runOutsideAngular(
    //   () => new ZoneScheduler(Zone.current)
    // );

    this.activities$
      .pipe(
        switchMap(
          /**
           * We need to override default `interval` scheduler in order to
           * manage current Zone stability
           */
          entries =>
            interval(this.interval).pipe(
              /**
               * Since `interval` operator returns an integer (`tick`),
               * we can use it to calculate an active item index
               */
              map(tick => tick % entries.length)
            )
        ),
        takeUntil(this.onDestroy$)
      )
      .subscribe(index => {
        this.activeActivityIndex = index;
        this.onActiveActivityChange.next(index);
        this.cdRef.detectChanges();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}

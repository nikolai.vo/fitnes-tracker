import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class UIService {

  constructor(private snackbar: MatSnackBar) {}

  showSnackbar(message, action, durations) {
    this.snackbar.open(message, action, {
      duration: durations
    });
  }
}

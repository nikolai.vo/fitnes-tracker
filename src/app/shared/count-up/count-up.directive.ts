import {
  Directive,
  ElementRef,
  Input,
  Output,
  HostListener,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { CountUp, CountUpOptions } from 'countup.js';

@Directive({
  selector: '[jmtCountUp]'
})
export class CountUpDirective implements OnChanges {
  countUp: CountUp;
  // the value you want to count to
  @Input('jmtCountUp') endVal: number;

  @Input('jmtCountUpOptions') options: CountUpOptions = {};
  @Input('jmtCountUpReanimateOnClick') reanimateOnClick = true;
  @Output('jmtCountUpComplete') complete = new EventEmitter<void>();

  constructor(private el: ElementRef) {}

  // Re-animate if preference is set.
  @HostListener('click')
  onClick() {
    if (this.reanimateOnClick) {
      this.animate();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.endVal && changes.endVal.currentValue) {
      this.countUp = new CountUp(
        this.el.nativeElement,
        this.endVal,
        this.options
      );
      this.animate();
    }
  }

  /** Add `value` to the end-value supplied to the `jmtCountUp` input */
  public updateValue(value: number) {
    if (!Number.isNaN(Number(value))) {
      this.endVal = this.endVal + value;
      this.countUp.update(this.endVal);
    }
  }

  private animate() {
    this.countUp.reset();
    this.countUp.start(() => {
      this.complete.emit();
    });
  }
}

import { Component, NgZone, Input, ViewChild, ElementRef } from '@angular/core';

enum ScrollerDirection {
  Left = 1,
  Right = 2
}

@Component({
  selector: 'app-horizontal-scroll',
  templateUrl: './horizontal-scroll.component.html',
  styleUrls: ['./horizontal-scroll.component.scss']
})
export class HorizontalScrollComponent {
  @ViewChild('scrollingWrapper', { static: false })
  private scrollingWrapper: ElementRef<HTMLElement>;

  @Input('ScrollerDuration')
  private ScrollerDuration = 200;

  constructor(private zone: NgZone) { }

  scrollIt(direct: 'left' | 'right') {
    const direction = this.getDirection(direct);
    const duration = this.ScrollerDuration;
    this.zone.runOutsideAngular(() => {
      const scrollStart = this.scrollingWrapper.nativeElement.scrollLeft;
      const startTime = new Date().getTime();

      const scroll = () => {
        requestAnimationFrame(() => {
          const t0 = startTime;
          const t = new Date().getTime();
          const pt = Math.min(t - t0, duration) / duration;
          const pe = pt * (2 - pt);
          this.scrollingWrapper.nativeElement.scrollLeft =
            direction === ScrollerDirection.Left
              ? scrollStart - 380 * pe
              : scrollStart + 380 * pe;

          if (pe < 1) {
            scroll();
          }
        });
      };

      scroll();
    });
  }

  private getDirection(value: 'left' | 'right') {
    if (!['left', 'right'].includes(value)) {
      throw new Error(
        'Invalid "jmtScrollerDirection" arg! Must be "left" or "right"'
      );
    }
    return value === 'left' ? ScrollerDirection.Left : ScrollerDirection.Right;
  }
}

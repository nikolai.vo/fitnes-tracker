import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { AvatarComponent } from './avatar/avatar.component';
import { AvatarListComponent } from './avatar-list/avatar-list.component';
import { HorizontalScrollComponent } from './horizontal-scroll/horizontal-scroll.component';
import { LoadingButtonComponent } from './loading-button/loading-button.component';
import { LatestActivityComponent } from './latest-activity/latest-activity.component';
import { CountUpDirective } from './count-up/count-up.directive';
import { DragAndDropComponent } from './drag-and-drop/drag-and-drop.component';
import { StepperComponent } from './stepper/stepper.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    AvatarComponent,
    AvatarListComponent,
    HorizontalScrollComponent,
    LoadingButtonComponent,
    LatestActivityComponent,
    CountUpDirective,
    DragAndDropComponent,
    StepperComponent
  ],
  declarations: [
    AvatarComponent,
    AvatarListComponent,
    HorizontalScrollComponent,
    LoadingButtonComponent,
    LatestActivityComponent,
    CountUpDirective,
    DragAndDropComponent,
    StepperComponent
  ],
  entryComponents: [
    StepperComponent
  ]
})
export class SharedModule { }

import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';

@Component({
  selector: 'loading-button',
  templateUrl: 'loading-button.component.html',
  styleUrls: ['./loading-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingButtonComponent implements OnInit {
  @Input('classes') classes: any = {
    'btn-primary': true
  };
  @Input('indicatorClasses') indicatorClasses: string;

  @Input('text') text: string;
  @Input('type') type: string = '';
  @Input('loading') loading: boolean = false;
  @Input('disabled') disabled: boolean = false;

  constructor() { }
  ngOnInit() {
  }
}

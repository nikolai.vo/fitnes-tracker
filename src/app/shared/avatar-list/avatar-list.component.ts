import {
  Component,
  ViewChild,
  ElementRef,
  Input,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-avatar-list',
  templateUrl: './avatar-list.component.html',
  styleUrls: ['./avatar-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarListComponent implements OnChanges {
  @ViewChild('avatarContainer', { static: false }) avatarContainer: ElementRef;
  @Input() avatars: Array<string>;
  @Input() disabled: boolean;
  @Input() numToDisplay: number;

  public avatarsList: Array<string> = null;
  public avatarActivity: number;
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.updateAvatarList(this.avatars, this.numToDisplay);
    }
  }

  updateAvatarList(avatars: Array<string>, numToDisplay?: number) {
    const setNumToDisplay = numToDisplay || 4;
    if (avatars.length > setNumToDisplay) {
      this.avatarsList = avatars
        .slice(0, setNumToDisplay)
        .reverse();
      // this.avatarsList = avatars
      // .sort()
      // .slice(0, setNumToDisplay)
      // .reverse();
      this.avatarActivity = avatars.length - setNumToDisplay;
    }
    if (avatars.length > 0 && avatars.length <= setNumToDisplay) {
      this.avatarsList = avatars.reverse();
      // this.avatarsList = avatars.sort().reverse();
      this.avatarActivity = null;
    }
  }
}
